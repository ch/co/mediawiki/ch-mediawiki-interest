Ch Mediawiki Interest
---------------------

This is simply a meta-package that provides no files beyond a postinst script
that gets triggered when `/usr/share/mediawiki` gets updated by dpkg as part of
the installation, removal or upgrade of another package.

It declares an "interest" in `/usr/share/mediawiki` via the dpkg triggers
mechanism.

The postinst script (when invoked due to the trigger) itself defers to the
`/usr/local/bin/run-wiki-updates` script that we put in place on our mediawiki
server(s) via ansible.

N.B. the postinst script currently does NOT return a failure code to dpkg if any
of the wikis failed their updates. This is mostly because I haven't fully
understood the implications of which packages will be left in which states under
these circumstances. But the details should be documented at
https://git.dpkg.org/cgit/dpkg/dpkg.git/tree/doc/spec/triggers.txt
